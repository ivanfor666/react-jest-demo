import React from "react";
import reactDom from "react-dom"
import CheckboxWithLabel from "./CheckboxWithLabel"
import LinkDemo from "./Link.react"

const App = () =>{
  return <div>
    <CheckboxWithLabel/>
    <LinkDemo page={"home"}>123</LinkDemo>
  </div>
}

reactDom.render(React.createElement(App),document.getElementById("root"))
