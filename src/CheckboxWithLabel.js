// Copyright 2004-present Facebook. All Rights Reserved.

import React, {useState} from 'react';

const CheckboxWithLabel = ({labelRef, inputRef, labelOn, labelOff}) => {
  const [isChecked, setIsChecked] = useState(false);
  // console.log({labelRef, inputRef, labelOn, labelOff})
  const onChange = () => {
    setIsChecked(!isChecked);
  };

  return (
    <label ref={labelRef}>
      <input
        ref={inputRef}
        type="checkbox"
        checked={isChecked}
        onChange={onChange}
      />
      {isChecked ? labelOn : labelOff}
    </label>
  );
};

export default CheckboxWithLabel;
