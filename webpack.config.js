const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: "development",
  entry: "./src/index.js",
  output: {path: path.resolve(__dirname, "dist"), filename: "[name].js",},
  module: {rules: [{test: /\.(js|jsx)$/, exclude: /node_modules/, loader: 'babel-loader'},]},
  resolve: {extensions: [".js", ".jsx", ".json", ".css"],},
  plugins: [new HtmlWebpackPlugin({title: "frontend-tutorial", template: "./index.html"}),],
  devServer: {contentBase: path.join(__dirname, 'dist'), port: 3034, hot: true,}
};
